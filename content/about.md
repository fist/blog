---
title: About FIST (Trichy GLUG)
date: 2016-05-01
author: subu-rg
category: Introduction
description: "Everything about TrichyGLUG"
tags: [about,introduction]
comments: false
share:
---

Trichy GLUG (GNU/Linux Users' Group) is a volunteer community functioning under FSFTN.

We believe in keeping the power of information and knowledge in people’s hands. We envision a world where knowledge is free* and fair.
We believe the amplification of community voices and building relationships can lead to that impact of spreading ideas beyond just one community, to greater sets of audience.

Legend has it that folks here bleed code and pixels.(You can’t blame us if you encounter ambidextrous and sapiosexual people)

We are a bunch of high spirited folks. 
We are a team.
You and me. 
We and us.

We are new, bountiful and infinite. 
We are a fresh generation of thinkers, movers and shakers. 
And we hope to succeed in our first attempt.

Bleed. Flow. And remember to contribute.

*- Let’s get rid of the asterixes.

Buckle up for your journey into the world of open source.
Start Today:
Matrix 
Facebook @trichy.fsftn
Twitter
